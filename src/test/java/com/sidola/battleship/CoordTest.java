package com.sidola.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sidola.battleship.board.Coord;

import junit.framework.TestCase;

public class CoordTest extends TestCase {

	private List<Coord> coordList = new ArrayList<Coord>();
	private Map<Coord, Integer> coordMap = new HashMap<Coord, Integer>();
	
	protected void setUp() throws Exception {
		super.setUp();
		
		coordList.add(new Coord(0, 1));
		coordList.add(new Coord(0, 2));
		coordList.add(new Coord(0, 3));
		
		coordMap.put(new Coord(0, 1), 1);
		coordMap.put(new Coord(0, 2), 2);
		coordMap.put(new Coord(0, 3), 3);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		
		coordList = null;
		coordMap = null;
	}

	public void testHashCode() {
		assertTrue( coordMap.containsKey(new Coord(0, 1)) );
		assertTrue( coordMap.containsKey(new Coord(0, 2)) );
		assertTrue( coordMap.containsKey(new Coord(0, 3)) );
	}

	public void testGetX() {
		Coord coord = new Coord(5, 5);
		assertTrue( coord.getX() == 5 );
	}

	public void testGetY() {
		Coord coord = new Coord(5, 5);
		assertTrue( coord.getY() == 5 );
	}

	public void testEqualsObject() {
		assertTrue( coordList.contains(new Coord(0, 1)) );
		assertTrue( coordList.contains(new Coord(0, 2)) );
		assertTrue( coordList.contains(new Coord(0, 3)) );
	}

}
