package com.sidola.battleship.ships;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.sidola.battleship.board.Coord;

public class ShipTest extends TestCase {

	Ship ship;
	
	protected void setUp() throws Exception {
		super.setUp();
		
		ship = new Destroyer();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		
		ship = null;
	}

	public void testShip() {
		assertTrue( ship.getName() == "Destroyer" );
		assertTrue( ship.getSize() == 3 );
	}

	public void testGetName() {
		assertTrue( ship.getName() == "Destroyer" );
	}

	public void testGetSize() {
		assertTrue( ship.getSize() == 3 );
	}

	public void testSetShipCoords() {
		List<Coord> shipCoordinates = new ArrayList<Coord>();
		
		shipCoordinates.add( new Coord(0, 1) );
		shipCoordinates.add( new Coord(0, 2) );
		shipCoordinates.add( new Coord(0, 3) );
		
		ship.setShipCoords(shipCoordinates);

		assertTrue( ship.getShipCoords().get(0).equals(new Coord(0, 1)) );
		assertTrue( ship.getShipCoords().get(1).equals(new Coord(0, 2)) );
		assertTrue( ship.getShipCoords().get(2).equals(new Coord(0, 3)) );		
	}

	public void testGetShipCoords() {
		this.testSetShipCoords();
	}

	public void testRemoveShipCoord() {
		this.testSetShipCoords();
		ship.removeShipCoord(new Coord(0, 1));
		
		assertTrue( ship.getShipCoords().size() == 2 );
		assertFalse( ship.getShipCoords().get(0).equals(new Coord(0, 1)) );
	}

	public void testIsShipDestroyed() {
		this.testSetShipCoords();
		
		ship.removeShipCoord( new Coord(0, 1) );
		ship.removeShipCoord( new Coord(0, 2) );
		assertFalse( ship.isShipDestroyed() );
		
		ship.removeShipCoord( new Coord(0, 3) );
		assertTrue( ship.isShipDestroyed() );
	}

}
