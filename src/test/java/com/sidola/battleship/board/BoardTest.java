package com.sidola.battleship.board;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class BoardTest extends TestCase {

	private Board board;
	
	protected void setUp() throws Exception {
		super.setUp();
		board = new Board();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		board = null;
	}
	
	public void testBoard() {
		String[][] layout = board.getLayout();
		assertTrue( layout.length == 10 );
		assertTrue( layout[0].length == 10 );
		
		String tile = board.getTile( new Coord(0, 5) );
		assertTrue(tile == Tile.WATER.getTile());
	}

	public void testGetLayout() {
		String[][] layout = board.getLayout();
		assertTrue( layout.length == 10 );
		assertTrue( layout[0].length == 10 );
	}

	public void testPlaceTile() {
		Coord coord = new Coord(0, 0);
		board.placeTile(Tile.HIT, coord);
		String tile = board.getTile(coord);
		assertTrue( tile == Tile.HIT.getTile() );
	}

	public void testPlaceShip() {
		List<Coord> shipCoordinates = new ArrayList<Coord>();
		shipCoordinates.add( new Coord(0, 0) );
		shipCoordinates.add( new Coord(0, 1) );
		shipCoordinates.add( new Coord(0, 2) );
		
		board.placeShip(shipCoordinates);
		
		String tile = board.getTile( new Coord(0, 0) );
		assertTrue( tile == Tile.SHIP.getTile() );
		
		tile = board.getTile( new Coord(0, 1) );
		assertTrue( tile == Tile.SHIP.getTile() );
		
		tile = board.getTile( new Coord(0, 2) );
		assertTrue( tile == Tile.SHIP.getTile() );
	}

	public void testGetTile() {
		board.placeTile(Tile.HIT, new Coord(0, 1));
		String tile = board.getTile(new Coord(0, 1));
		assertTrue( tile == Tile.HIT.getTile() );
	}

	public void testIsCoordOutOfBounds() {
		Coord coord = new Coord(0, 11);
		assertTrue( board.isCoordOutOfBounds(coord) );
		
		coord = new Coord(11, 0);
		assertTrue( board.isCoordOutOfBounds(coord) );
		
		coord = new Coord(25, 30);
		assertTrue( board.isCoordOutOfBounds(coord) );
		
		coord = new Coord(5, 5);
		assertFalse( board.isCoordOutOfBounds(coord) );
	}
}
