import com.sidola.battleship.network.Server;


public class ServerApplication {

	public static void main(String[] args) {
		
		Server server = new Server();
		server.start();
		
	}
	
}
