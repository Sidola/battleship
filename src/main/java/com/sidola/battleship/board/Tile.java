package com.sidola.battleship.board;

public enum Tile {

	WATER(" "),
	HIT("X"),
	MISS("O"),
	SHIP("B");
	
	String tile;
	
	Tile(String tile) {
		this.tile = tile;
	}
	
	public String getTile() {
		return tile;
	}
	
}
