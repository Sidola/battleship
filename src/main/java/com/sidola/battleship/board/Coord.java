package com.sidola.battleship.board;

import java.io.Serializable;

public class Coord implements Serializable {

	private int x;
	private int y;
	
	public Coord(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Returns the x coordinate
	 * @return
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Returns the y coordinate
	 * @return
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Override equals method to only match the coords	
	 */
	@Override
	public boolean equals(Object o) {
		Coord c = (Coord) o;
		if ((c.x == this.x) && (c.y == this.y)) return true;
		return false;
	}
	
	/**
	 * Override hashmap to return a unique id
	 * based on the two coordinates
	 */
	@Override
	public int hashCode() {
		return Integer.parseInt(x + "" + y);
	}

}

