package com.sidola.battleship.board;

import java.util.Arrays;
import java.util.List;

public class Board {

	private String[][] layout;
	
	/**
	 * Creates a board and fills it with tiles of type WATER
	 */
	public Board() {
		layout = new String[10][10];
		
		for (String[] col : layout) {
			Arrays.fill(col, Tile.WATER.getTile());
		}
	}
	
	/**
	 * Returns the current board-layout
	 * @return String[][]
	 */
	public String[][] getLayout() {
		return layout;
	}
	
	/**
	 * Places a tile at the given coordinates on the board
	 * @param tileType - Type of tile to place on the board
	 * @param coord - Coordinate at which to place the tile
	 */
	public void placeTile(Tile tileType, Coord coord) {
		layout[ coord.getY() ][ coord.getX() ] = tileType.getTile();
	}
	
	/**
	 * Places an array of ship coordinates on the board
	 * @param shipCoordinates - ArrayList containing coordinates for a ship
	 */
	public void placeShip(List<Coord> shipCoordinates) {
		for (Coord coord : shipCoordinates) {
			this.placeTile(Tile.SHIP, coord);
		}
	}
	
	/**
	 * Returns the tile at the given coordinate
	 * @param coord - Coordinate of the tile you want to get
	 * @return Return the tile-type at this coordinate
	 */
	public String getTile(Coord coord) {
		return layout[ coord.getY() ][ coord.getX() ];
	}
	
	/**
	 * Checks if the given coordinate is out of bounds
	 * @param coord - Coordinate to check
	 * @return true if the coordinate is out of bounds, false if not
	 */
	public Boolean isCoordOutOfBounds(Coord coord) {
		if (coord.getX() < 0) return true;
		if (coord.getX() > layout.length) return true;
		
		if (coord.getY() < 0) return true;
		if (coord.getY() > layout[0].length) return true;
		
		return false;
	}
	
}






