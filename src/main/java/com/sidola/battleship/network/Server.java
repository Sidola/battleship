package com.sidola.battleship.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

	final private int PORT = 4242;
	final private List<ClientThread> clientList = new ArrayList<ClientThread>();
	final private List<ObjectOutputStream> clientOutputList = new ArrayList<ObjectOutputStream>();
	
	/**
	 * Starts the server
	 */
	public void start() {
		try {
			
			// Create a server socket
			ServerSocket serverSocket = new ServerSocket(PORT);
			
			System.out.println("Server started - Listening for clients");
			
			// Go into infinite loop and listen for connection requests
			while(true) {
				
				Socket clientSocket = serverSocket.accept();
				System.out.println("Client connected");
				
				// Get the streams
				ObjectOutputStream outputStream = new ObjectOutputStream(clientSocket.getOutputStream());
				outputStream.flush(); // Flush upon creation because "reasons"?
				ObjectInputStream inputStream = new ObjectInputStream(clientSocket.getInputStream());
				
				// Create a new client object
				ClientThread client = new ClientThread(inputStream, outputStream);
				
				// Store the client until we have two
				clientList.add(client);
				clientOutputList.add(outputStream);
				
				// Start a new thread for the client
				Thread t = new Thread(client);
				t.start();
				
				System.out.println("New client thread started");
				
				// If we have two clients running, connect them
				if (clientList.size() == 2) {
										
					for (ObjectOutputStream objectOutputStream : clientOutputList) {
						objectOutputStream.writeObject( new Message() );
						objectOutputStream.flush();
					}			
					
					System.out.println("Sent confirmation to clients");			
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	/**
	 * Sends data to all clients except the one sending the data
	 * @param obj
	 * @param sendingOuput
	 */
	private void sendToPartner(Object obj, ObjectOutputStream sendingOuput) {
						
		try {
			for (ObjectOutputStream objectOutputStream : clientOutputList) {
				if (objectOutputStream.equals(sendingOuput)) continue;
				objectOutputStream.writeObject(obj);				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Inner class for the client threads
	 * @author Sid
	 *
	 */
	private class ClientThread implements Runnable {

		private ObjectInputStream inputStream;
		private ObjectOutputStream outputStream;
		
		/**
		 * Constructor, takes the input & output for the client
		 * @param input
		 * @param output
		 * @throws IOException 
		 */
		public ClientThread(ObjectInputStream input, ObjectOutputStream output) throws IOException {
			inputStream = input;
			outputStream = output;
		}
				
		/**
		 * Entry-point for the thread
		 */
		public void run() {
			try {
				while(true) {
					Object obj = inputStream.readObject();			
					sendToPartner(obj, outputStream);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
}
