package com.sidola.battleship.network;

import com.sidola.battleship.board.Coord;

public class CoordMessage extends Message {

	private Coord coord;
	
	/**
	 * Sets the coord
	 * @param coord
	 */
	public CoordMessage(Coord coord) {
		this.coord = coord;
	}
	
	/**
	 * Gets the coord
	 * @return
	 */
	public Coord getCoord() {
		return coord;
	}
	
}
