package com.sidola.battleship.network;

import java.util.HashMap;
import java.util.Map;

import com.sidola.battleship.board.Coord;
import com.sidola.battleship.ships.Ship;

public class ShipMapMessage extends Message {

	private Map<Coord, Ship> shipMap = new HashMap<Coord, Ship>();
	
	/**
	 * Sets the shipmap
	 * @param shipMap
	 */
	public ShipMapMessage(Map<Coord, Ship> shipMap) {
		this.shipMap = shipMap;
	}
	
	/**
	 * Returns the shipmap
	 * @return
	 */
	public Map<Coord, Ship> getMap() {
		return shipMap;
	}
	
}
