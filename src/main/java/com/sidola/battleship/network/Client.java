package com.sidola.battleship.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Client {

	private ObjectInputStream inputStream;
	private ObjectOutputStream outputStream;
	private InputBufferThread inputBuffer;
	
	private Socket clientSocket;
	
	private final String HOST = "127.1.0.0";
	private final int PORT = 4242;
	
	private List<Object> bufferList = new ArrayList<Object>();
	
	/**
	 * Connects to a server and waits for a partner
	 */
	public void connect() {
				
		try {
			clientSocket = new Socket(HOST, PORT);
			
			// Create streams
			outputStream = new ObjectOutputStream(clientSocket.getOutputStream());
			outputStream.flush(); // Flush upon creation because "reasons"?
			inputStream = new ObjectInputStream(clientSocket.getInputStream());
			
			// Stay here until we hear back from the server
			Object obj = inputStream.readObject();
						
			// Create a new input buffer
			inputBuffer = new InputBufferThread(inputStream);
			// Throw the input buffer in its own thread
			Thread t = new Thread(inputBuffer);
			t.start();
									
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Sends data to the server
	 * @param o
	 */
	public void sendMessage(Object o) {
		try {
			outputStream.writeObject(o);
			outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Waits for the server to send data back. Will block until data is received.
	 * @return
	 */
	public Object getMessage() {
		Object obj = null;
		
		// Block until we get a message from the buffer
		while((obj = getBuffer()) == null) {
			continue;
		}
		
		return obj;
	}
		
	/**
	 * Puts data in the buffer
	 * @param o
	 */
	private void setBuffer(Object o) {
		bufferList.add(o);
	}
	
	/**
	 * Gets data from the buffer
	 * @return
	 */
	private Object getBuffer() {
		if (bufferList.isEmpty()) return null;
				
		Object b = bufferList.get(0);
		bufferList.remove(0);
		return b;
	}
	
	/**
	 * Inner class to handle input-buffering
	 * @author Sid
	 *
	 */
	private class InputBufferThread implements Runnable {
		
		private ObjectInputStream inputStream;
		private Object buffer = null;
		
		/**
		 * Constructor. Takes the inputStream from the server.
		 * @param input
		 */
		public InputBufferThread(ObjectInputStream input) {
			inputStream = input;
		}
		
		/**
		 * Entry-point for the thread. Only listens to input from the server
		 * and buffers it for later retrieval. Only buffers ONE message.
		 */
		public void run() {
			try {
				while(true) {					
					buffer = inputStream.readObject();
					setBuffer(buffer);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
	}
	
}









