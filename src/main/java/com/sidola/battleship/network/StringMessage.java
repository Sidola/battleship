package com.sidola.battleship.network;


public class StringMessage extends Message {

	private String message;
	
	/**
	 * Sets the message
	 * @param message
	 */
	public StringMessage(String message) {
		this.message = message;
	}
	
	/**
	 * Gets the message
	 * @return
	 */
	public String getMessage() {
		return message;
	}
	
}
