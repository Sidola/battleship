package com.sidola.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sidola.battleship.IO.TerminalController;
import com.sidola.battleship.IO.TerminalView;
import com.sidola.battleship.board.Board;
import com.sidola.battleship.board.Coord;
import com.sidola.battleship.board.Tile;
import com.sidola.battleship.network.Client;
import com.sidola.battleship.network.CoordMessage;
import com.sidola.battleship.network.ShipMapMessage;
import com.sidola.battleship.network.StringMessage;
import com.sidola.battleship.ships.AircraftCarrier;
import com.sidola.battleship.ships.Destroyer;
import com.sidola.battleship.ships.PatrolBoat;
import com.sidola.battleship.ships.Ship;
import com.sidola.battleship.ships.Submarine;

public class Game {

	private final TerminalController controller = new TerminalController();
	private final TerminalView view = new TerminalView();
	
	private String playerName, opponentName;
	private Board playerBoard, opponentBoard;
	
	private List<Ship> playerShipList = new ArrayList<Ship>();
	private List<Coord> usedCoordinates = new ArrayList<Coord>();
	private List<Coord> attackedCoordinates = new ArrayList<Coord>();
	private Map<Coord, Ship> playerShipMap = new HashMap<Coord, Ship>();
	private Map<Coord, Ship> opponentShipMap = new HashMap<Coord, Ship>();
	
	private Client client = new Client();
	
	/**
	 * Initiates the game
	 */
	public void bootGame() {
		this.initGame();	
	}

	/**
	 * Entry-point for the game
	 */
	private void initGame() {
		// Give the output writer to the view object
		view.setWriter( controller.getWriter() );
		
		// Create the boards
		playerBoard = new Board();
		opponentBoard = new Board();
		
		// Seed the shipList
		playerShipList = this.seedShipList();
		
		// Welcome the player to the game
		controller.clear();
		view.printWelcomeMessage();
		playerName = controller.getPlayerName();
		controller.clear();
		
		// Ask the player to place their ship
		this.placeShips();
		controller.clear();
		
		// Look for a partner
		view.printMatchMakingMessage(playerName);		
		this.findOpponent();
		controller.clear();
				
		// Start the game
		this.startAttacking();
	}
	
	/**
	 * Handles the network-part of the game.
	 * Is responsible for finding us a partner.
	 */
	private void findOpponent() {
		// Connect to the server
		client.connect();
		
		// Send our name
		StringMessage outMessage = new StringMessage(playerName);
		client.sendMessage(outMessage);
		
		// Wait for the opponent's name
		StringMessage message = (StringMessage) client.getMessage();
		opponentName = message.getMessage();
		
		// Send our shipmap
		ShipMapMessage outMap = new ShipMapMessage(playerShipMap);
		client.sendMessage(outMap);
		
		// Wait for the opponent's map
		ShipMapMessage shipMapMessage = (ShipMapMessage) client.getMessage();
		opponentShipMap.putAll(shipMapMessage.getMap());		
	}

	/**
	 * Takes care of placing ships on the board
	 */
	private void placeShips() {
		
		view.printBoardLayout(playerBoard.getLayout());
		view.printPlacementInstructions();
		
		// Iterate over the shiplist and place ships
		for (Ship ship : playerShipList) {
			
			String shipName = ship.getName();
			int shipSize = ship.getSize();
			List<Coord> shipCoordinates = new ArrayList<Coord>();
			
			view.printShipToPlace(shipName, shipSize);

			// Keep looping until we've got a valid placement
			while(true) {
				
				shipCoordinates = controller.getPlayerShipPlacement(shipSize);
				
				if (!isValidShipCoords(shipCoordinates)) {
					view.printInvalidShipPlacement();
					view.printEnterCoordinateAndOrientation();
					continue;
				}
				
				// Place the ship on the board
				playerBoard.placeShip(shipCoordinates);
				
				// Give the coordinates to the ship itself
				ship.setShipCoords(shipCoordinates);
				
				// Block out the coords for future use and store
				// them in the shipMap
				blockCoordinates(shipCoordinates, ship);
				
				// Break out of the loop
				break;
			}

			controller.clear();
			view.printBoardLayout(playerBoard.getLayout());
			
		} // Fore-loop ends
	}

	/**
	 * Adds the given coordinates to the blocklist and shipmap
	 * @param shipCoordinates
	 * @param ship
	 */
	private void blockCoordinates(List<Coord> shipCoordinates, Ship ship) {
		for (Coord coord : shipCoordinates) {
			// Store the coordinates in the shipmap
			playerShipMap.put(coord, ship);
			
			// Block out the coords for future ships
			// Not exactly the most efficient method to do it...
			usedCoordinates.add(coord); // Block main coord
			usedCoordinates.add(new Coord(coord.getX() + 1, coord.getY())); // Block one coord to the left
			usedCoordinates.add(new Coord(coord.getX() - 1, coord.getY())); // Block one coord to the right
			usedCoordinates.add(new Coord(coord.getX(), coord.getY() + 1)); // Block one coord above
			usedCoordinates.add(new Coord(coord.getX(), coord.getY() - 1)); // Block one coord below
		}		
	}

	/**
	 * This method handles the attack logic throughout the game
	 */
	private void startAttacking() {
		view.printBoardLayout(playerBoard.getLayout());
		view.printBoardLayout(opponentBoard.getLayout());

		view.printMatchFound(opponentName);
		view.printSeparator();
		
		view.printAttackInstructions();
		Coord playerCoord;
		Coord opponentCoord;
		
		while(!opponentShipMap.isEmpty() || (!playerShipMap.isEmpty())) {
			
			// Get input and make sure it's valid
            while(true) {
            	playerCoord = controller.getPlayerCoord();
                
                if (playerBoard.isCoordOutOfBounds(playerCoord)) {
                    view.printInvalidAttackCoordinate();
                    view.printEnterCoordinate();
                    continue;
                }
                
                if (attackedCoordinates.contains(playerCoord)) {
                    view.printInvalidAttackCoordinate();
                    view.printEnterCoordinate();
                    continue;
                }   
                break;
            }
			
            // Add this coordinate to the list of attacked coordinates
            attackedCoordinates.add(playerCoord);
            
            // Attack the opponent
            Ship opponentShip = this.attackCoordinate(playerCoord, opponentShipMap, opponentBoard);
            
            // ------------------------
            
            // Our turn is done
            // Send our attack coordinates to our opponent and wait for his attack
            view.printPendingAttack(); 
            
            // ------------------------ 
            
            // Send the coordinate
            CoordMessage coordToSend = new CoordMessage(playerCoord);
            client.sendMessage(coordToSend);
                        
            // Wait for the opponent's coordinate
            CoordMessage coordFromOpponent = (CoordMessage) client.getMessage();
            opponentCoord = coordFromOpponent.getCoord();
            
            // Attack the player
            Ship playerShip = this.attackCoordinate(opponentCoord, playerShipMap, playerBoard);
                        
            // ------------------------ 
            // Now that we know how both players attacked
            // We can give them some feedback
            // ------------------------ 
            controller.clear();
            
            // Print the boards first
            view.printBoardLayout(playerBoard.getLayout());
            view.printBoardLayout(opponentBoard.getLayout());
            
            // We hit a ship
            if (opponentShip != null) {
            	view.printPlayerHitShip(opponentShip.getName());
            	if (opponentShip.isShipDestroyed()) {
            		view.printPlayerDestroyedShip(opponentShip.getName());
            	}
            } else {
            	view.printPlayerMissed();
            }
            
            // If our ship was hit
            if (playerShip != null) {
            	view.printOpponentHitShip(playerShip.getName(), opponentCoord);
            	if (playerShip.isShipDestroyed()) {
            		view.printOpponentDestroyedShip(playerShip.getName());
            	}
            } else {
            	view.printOpponentMissed(opponentCoord);
            }
            
            // If the game is over
            if (playerShipMap.isEmpty()) {
            	view.printPlayerLoses();
            	break;
            } else if (opponentShipMap.isEmpty()) {
            	view.printPlayerWins();
            	break;
            }

            // Ask the player to enter a new coordinate
            view.printSeparator();
            view.printEnterCoordinate();
            
		} // Turn ended	
	}
	
	/**
	 * Attacks a coordinate on the board. If the coordinate is occupied by a ship
	 * it removes that coordinate from the ship.
	 * @param coord
	 * @param shipMap
	 * @param board
	 * @return returns the ship that was hit if any, otherwise null
	 */
	private Ship attackCoordinate(Coord coord, Map<Coord, Ship> shipMap, Board board) {
        Tile tileToPlace;
        Ship ship = null;
        
        // Check if the attack hit a coordinate
        if (shipMap.containsKey(coord)) {
        	tileToPlace = Tile.HIT;
        	
        	// Get the ship at this coord
        	ship = shipMap.get(coord);
        	
        	// Remove the coord from that ship and the map
        	ship.removeShipCoord(coord);
        	shipMap.remove(coord);      	
        } else {
        	// Place a miss-tile if we missed and return null
        	tileToPlace = Tile.MISS;
        }
		
        // Place the tile on the board
        board.placeTile(tileToPlace, coord);
        
        return ship;
	}
	
	/**
	 * Creates a list of ships to be used in the game
	 * @return
	 */
	private List<Ship> seedShipList() {
		List<Ship> shipList = new ArrayList<Ship>();
		
		shipList.add( new PatrolBoat() );
		shipList.add( new PatrolBoat() );
//		shipList.add( new Destroyer() );
//		shipList.add( new Submarine() );
//		shipList.add( new AircraftCarrier() );
		
		return shipList;
	}
	
	/**
	 * Checks if a ship is being placed on valid coordinates
	 * @param shipCoordinates
	 * @return
	 */
	private Boolean isValidShipCoords(List<Coord> shipCoordinates) {
		for (Coord coord : shipCoordinates) {
			if (playerBoard.isCoordOutOfBounds(coord)) return false;
			if (usedCoordinates.contains(coord)) return false;
		}
		
		return true;
	}
}










