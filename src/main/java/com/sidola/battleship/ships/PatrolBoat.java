package com.sidola.battleship.ships;

public class PatrolBoat extends Ship {
	
	public PatrolBoat() {
		super("Patrol Boat", 2);
	}
	
}
