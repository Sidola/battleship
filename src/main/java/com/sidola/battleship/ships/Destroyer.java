package com.sidola.battleship.ships;

public class Destroyer extends Ship {

	public Destroyer() {
		super("Destroyer", 3);
	}

}
