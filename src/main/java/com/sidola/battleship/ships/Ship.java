package com.sidola.battleship.ships;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.sidola.battleship.board.Coord;

public abstract class Ship implements Serializable {

	private String shipName;
	private int shipSize;
	private List<Coord> shipCoordiantes = new ArrayList<Coord>();
	
	public Ship(String name, int size) {
		shipName = name;
		shipSize = size;
	}
	
	/**
	 * Returns the name of the ship
	 * @return
	 */
	public String getName() {
		return shipName;
	}
	
	/**
	 * Returns the size of the ship
	 * @return
	 */
	public int getSize() {
		return shipSize;
	}

	/**
	 * Sets the coordinates for the ship
	 * @param shipCoordinates - ArrayList with Coord objects the ship will occupy
	 */
	public void setShipCoords(List<Coord> shipCoordinates) {
		this.shipCoordiantes = shipCoordinates;
	}
	
	/**
	 * Returns the shipCoordiantes
	 * @return
	 */
	public List<Coord> getShipCoords() {
		return shipCoordiantes;
	}
	
	/**
	 * Removes a coordinate from the shipCoordinates
	 * @param coord - A Coord object that a ship currently covers
	 */
	public void removeShipCoord(Coord coord) {
		shipCoordiantes.remove( shipCoordiantes.indexOf(coord) );
	}
	
	/**
	 * Checks if a ship is out of coordinates
	 * @return
	 */
	public Boolean isShipDestroyed() {
		if (shipCoordiantes.isEmpty()) return true;
		return false;
	}	
}
