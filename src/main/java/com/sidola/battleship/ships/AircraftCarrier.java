package com.sidola.battleship.ships;

public class AircraftCarrier extends Ship {

	public AircraftCarrier() {
		super("Aircraft Carrier", 5);
	}

}
