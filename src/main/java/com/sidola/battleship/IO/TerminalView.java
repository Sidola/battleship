package com.sidola.battleship.IO;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import com.sidola.battleship.board.Coord;
import com.sidola.battleship.board.Tile;

public class TerminalView {

	private final Map<Character, Integer> letterMapCharKey = new HashMap<Character, Integer>();
	private final Map<Integer, Character> letterMapIntKey = new HashMap<Integer, Character>();
	
	private PrintWriter writer;
	
	private final String CHAT_SEPARATOR = "--------------------------------------------";
	private final String BOARD_EDGE = "   -----------------------------------------";
	private final String CELL_SEPARATOR = " | ";
	
	public TerminalView() {		
		/**
		 * Create two maps to convert chars to ints and ints back to chars
		 * 
		 * http://stackoverflow.com/questions/2208688/quickest-way-to-enumerate-the-alphabet-in-c-sharp
		 */
		for (char c = 'A'; c <= 'Z'; c++) {
			letterMapCharKey.put(c, letterMapCharKey.size() + 1);
			letterMapIntKey.put(letterMapIntKey.size() + 1, c);
		}
		
	}

	/**
	 * Sets the output writer to be used
	 * @param w
	 */
	public void setWriter(PrintWriter w) {
		writer = w;
	}
		
	/**
	 * Wrapper for the println method
	 * @param string
	 */	
	private void println(Object o) {
		writer.println(o);
		writer.flush();
	}
	
	/**
	 * Wrapper for the print method
	 * @param o
	 */
	private void print(Object o) {
		writer.print(o);
		writer.flush();
	}
	
	/**
	 * Prints a board layout to the terminal
	 * @param layout
	 */
	public void printBoardLayout(String[][] layout) {
		// Iterate over the chars and print them above the board
		print("  "); // Left-side margin
		for (Map.Entry<Character, Integer> entry : letterMapCharKey.entrySet()) {
			print("   " + entry.getKey());
			
			// Break once we cover the board's width
			if (entry.getValue() >= layout.length) break;
		}
		
		// Linebreak
		println("");
		
		// Print the top edge
		println(BOARD_EDGE);
		
		for (int i = 0; i < layout.length; i++) {
			
			// Print the numbers on the left side
			// If the numbers are below 10, add a space for formatting
			if (i + 1 < layout.length) {
				print(i + 1 + " ");
			} else {
				print(i + 1);
			}
			
            // Print the actual board
            print(CELL_SEPARATOR);
            for (int j = 0; j < layout[i].length; j++) {
                
                String thisTile = layout[i][j];
                String tileToPrint = null;
                
                // Color the tiles
                if (thisTile == Tile.SHIP.getTile()) {
                    tileToPrint = JlineColors.GREEN.colorString(thisTile);
                } else if (thisTile == Tile.HIT.getTile()) {
                    tileToPrint = JlineColors.RED.colorString(thisTile);
                } else if (thisTile == Tile.MISS.getTile()) {
                    tileToPrint = JlineColors.CYAN.colorString(thisTile);
                } else {
                    tileToPrint = thisTile;                 
                }
                
                print(tileToPrint + CELL_SEPARATOR);
            }
			
			// Linebreak
			println("");
			
			// Print the bottom edge
			println(BOARD_EDGE);	
		}
		
		// Bottom-margin for text
		println("");
		
	}
	
	public void printBoardLayouts(String[][] layout1, String[][] layout2) {
		
		// TODO - Finish this...
		
		// Print out two lines of letters
		print("  "); // Left-side margin
		for (Map.Entry<Character, Integer> entry : letterMapCharKey.entrySet()) {
			print("   " + entry.getKey());
			
			// Break once we cover the board's width
			if (entry.getValue() >= layout1.length) break;
		}
		
		// Print out two lines of letters
		print("\t"); // Left-side margin
		for (Map.Entry<Character, Integer> entry : letterMapCharKey.entrySet()) {
			print("   " + entry.getKey());
			
			// Break once we cover the board's width
			if (entry.getValue() >= layout2.length) break;
		}
		
		println(""); // TODO - Remove
	}
	
	public void printWelcomeMessage() {
		String s = "Welcome to a game of Battleship";
		println(s);
		
		s = "Please enter your name:";
		println(s);
	}

	public void printMatchMakingMessage(String playerName) {
		String s = "Hello, " + playerName;		
		println(s);
		
		s = "Please wait while we find an opponent for you.";
		println(s);	
	}

	public void printMatchFound(String opponentName) {
		println("A match was found!");
		println("You will be playing against " + opponentName + ".");
	}

	public void printPlacementInstructions() {
		println("Please place your ships on the board.");
		println(CHAT_SEPARATOR);
		println("Expected input format is: XY ORIENTATION.");
		println("Example: B5 LEFT");
		println(CHAT_SEPARATOR);
		
	}

	public void printShipToPlace(String name, int size) {
		println("Placing ship:\t" + name);
		println("Ship size:\t" + size);
		println(CHAT_SEPARATOR);
		println("Enter coordinates & orientation:");
	}

	public void printInvalidShipPlacement() {
		println(JlineColors.RED.colorString("Invalid ship placement, try again."));
	}

	public void printEnterCoordinateAndOrientation() {
		println("Enter coordinates & orientation: ");
	}

	public void printAttackInstructions() {
		println("It's time to attack the enemy.");
		println("Enter a coordinate to attack: ");
	}

	public void printEnterCoordinate() {
		println("Enter a coordinate to attack: ");
	}
	
	public void printInvalidAttackCoordinate() {
		println(JlineColors.RED.colorString("Invalid attack coordinate, try again."));
	}
	
	public void printPlayerDestroyedShip(String shipName) {
		println("You destroyed a " + shipName + ".");
	}

	public void printPlayerWins() {
		println("You won!");
	}

	public void printPlayerHitShip(String shipName) {
		println("You hit a " + shipName + ".");		
	}
	
	public void printPlayerMissed() {
		println("You missed.");
	}

	public void printPlayerLoses() {
		println("You've lost.");		
	}

	public void printOpponentHitShip(String name, Coord opponentCoord) {
		int xInt = opponentCoord.getX() + 1;
		String xString = String.valueOf(letterMapIntKey.get(xInt));
		println("Your opponent hit your " + name + ". At coord: " + xString + "" + (opponentCoord.getY() + 1) + ".");
	}

	public void printOpponentDestroyedShip(String name) {
		println("Your opponent destroyed your " + name + ".");		
	}

	public void printOpponentMissed(Coord opponentCoord) {
		int xInt = opponentCoord.getX() + 1;
		String xString = String.valueOf(letterMapIntKey.get(xInt));
		println("Your opponent attacked " +  xString + "" + (opponentCoord.getY() + 1) + " and missed.");
	}

	public void printPendingAttack() {
		println("Waiting for the opponent to attack...");
	}

	public void printSeparator() {
		println(CHAT_SEPARATOR);
	}
	
}












