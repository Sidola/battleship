package com.sidola.battleship.IO;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jline.console.ConsoleReader;

import com.sidola.battleship.board.Coord;

public class TerminalController {

	private final Map<Character, Integer> letterMapCharKey = new HashMap<Character, Integer>();
	private final Map<Integer, Character> letterMapIntKey = new HashMap<Integer, Character>();
	
	private ConsoleReader jLineReader;
	
	public TerminalController() {
		
		// Init a jLine2 ConsoleReader
		try {
			jLineReader = new ConsoleReader();			
			jLineReader.setPrompt("> ");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/**
		 * Create two maps to convert chars to ints and ints back to chars
		 * 
		 * http://stackoverflow.com/questions/2208688/quickest-way-to-enumerate-the-alphabet-in-c-sharp
		 */
		for (char c = 'A'; c <= 'Z'; c++) {
			letterMapCharKey.put(c, letterMapCharKey.size() + 1);
			letterMapIntKey.put(letterMapIntKey.size() + 1, c);
		}
	}
	
	/**
	 * Returns the writer for this jLine2 instance
	 * @return
	 */
	public PrintWriter getWriter() {
		return new PrintWriter(jLineReader.getOutput());
	}
	
	/**
	 * Clears the screen
	 */
	public void clear() {
		try { jLineReader.clearScreen();
		} catch (IOException e) { e.printStackTrace(); }
	}
	
	/**
	 * Gets a coordinate from the player
	 * @return
	 */
	public Coord getPlayerCoord() {
		Coord coord = null;
		String input = null;
		
		Pattern pattern = Pattern.compile("^([A-Z]{1})(\\d{1,2})$");
		
		// Infinite loop until the input is valid
		while(true) {
			input = getInput().toUpperCase();
			Matcher matcher = pattern.matcher(input);
			
			// If the matcher didn't find anything, return an invalid coordinate
			if (!matcher.find()) {
				Coord invalidCoord = new Coord(-1, -1);
				return invalidCoord;
			}
			
			// Create a coordinate from the input
			coord = this.getCoordFromInput(matcher);
			
			// Break out of the loop
			break;
		}
		
		return coord;
	}

	/**
	 * Gets a coordinate and orientation from the player
	 * @return 
	 */
	public List<Coord> getPlayerShipPlacement(int shipSize) {
		String input = null;
		List<Coord> shipCoordinates = new ArrayList<Coord>();
		
		// Looks for valid input
		Pattern pattern = Pattern.compile("^([A-Z]{1})(\\d{1,2})\\s((UP|DOWN|LEFT|RIGHT))");
		
		// Infinite loop until the input is valid
		while(true) {
			// Get input from the user
			input = getInput().toUpperCase();
			
			// Run the given input on the set pattern
			Matcher matcher = pattern.matcher(input);
			
			// If the matcher didn't find anything, return an invalid coordinate
			if (!matcher.find()) {
				List<Coord> invalidCoordList = new ArrayList<Coord>(); 
				invalidCoordList.add( new Coord(-1, -1) );
				return invalidCoordList;
			}
			
			// Create a coordinate from the input
			Coord placementCoord = this.getCoordFromInput(matcher);
			// Convert the string direction to a Direction object
			Direction direction = this.getDirection(matcher.group(3));
			
			// Add the coordinates to the ship
			shipCoordinates.add(placementCoord);
			shipCoordinates.addAll( this.getShipCoords(shipSize, direction, placementCoord) );
			
			// Break out of the loop
			break;
		}
		
		return shipCoordinates;
	}

	/**
	 * Gets a name from the player
	 * @return
	 */
	public String getPlayerName() {
		return getInput();
	}
	
	/**
	 * Wrapper for the actual input method
	 * @return
	 */
	private String getInput() {
		try {
			// TODO - Is it possible to clear whatever has been typed already before asking the user to type more?
			// Currently this will buffer anything written in the console window
			return jLineReader.readLine();
		} catch (IOException e) { e.printStackTrace(); return null; }
	}
	
	/**
	 * Converts a letter to it's respective digit
	 * @param x
	 * @return
	 */
	private int convertLetterCoordToInteger(String x) {		
		int xInt = letterMapCharKey.get(x.toUpperCase().charAt(0));
		return xInt;
	}
	
	/**
	 * Converts the direction string from the user to a true direction
	 * @param input
	 * @return A direction of the type Direction
	 */
	private Direction getDirection(String input) {
		Direction direction = null;
		
		switch (input) {
			case "UP": 		direction = Direction.UP; break;
			case "DOWN": 	direction = Direction.DOWN; break;
			case "LEFT": 	direction = Direction.LEFT; break;
			case "RIGHT": 	direction = Direction.RIGHT; break;
			default: 		direction = Direction.DOWN; break;
		}
		
		return direction;
	}
	
	/**
	 * Generates all the coords the given ship will occupy if placed at the given coord
	 * with the given orientation
	 * @param ship
	 * @param direction
	 * @param pickedCoord
	 * @return arraylist with coords for the ship
	 */
	private ArrayList<Coord> getShipCoords(int shipSize, Direction direction, Coord pickedCoord) {
		ArrayList<Coord> shipCoords = new ArrayList<Coord>();
		
		switch (direction) {
		case UP:
			for (int i = 1; i < shipSize; i++) {
				shipCoords.add( new Coord(pickedCoord.getX(), pickedCoord.getY() - i) );	
			}
			break;
		case DOWN:
			for (int i = 1; i < shipSize; i++) {
				shipCoords.add( new Coord(pickedCoord.getX(), pickedCoord.getY() + i) );	
			}
			break;
		case LEFT:
			for (int i = 1; i < shipSize; i++) {
				shipCoords.add( new Coord(pickedCoord.getX() - i, pickedCoord.getY()) );	
			}
			break;
		case RIGHT:
			for (int i = 1; i < shipSize; i++) {
				shipCoords.add( new Coord(pickedCoord.getX() + i, pickedCoord.getY()) );	
			}
			break;
		}
		
		return shipCoords;
	}
	
	/**
	 * Creates a coordinate object based on the input caught by the matcher
	 * @param matcher
	 * @return a Coord object
	 */
	private Coord getCoordFromInput(Matcher matcher) {
		Coord retCoord = null;
		
		String xAxisString = matcher.group(1);
		int xAxis = this.convertLetterCoordToInteger(xAxisString);
		int yAxis = Integer.parseInt(matcher.group(2));
		
		// Subtract 1 from each axis to account for the zero-index array format
		// of the board
		--xAxis; --yAxis;
		
		retCoord = new Coord(xAxis, yAxis);
		return retCoord; 
	}
	
}









